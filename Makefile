GENERATED=fdb_latexmk fls log aux out tex toc tex~ html~
ALL_GENERATED=pdf fdb_latexmk fls log aux out tex toc html tex~ html~

.PHONY: clean all tarball
.DEFAULT_GOAL = all

all: sops.pdf sops.html


%.tex : %.org
	emacs -Q $< --batch -f org-latex-export-to-latex --kill

%.html: %.org
	emacs -Q $< --batch -f org-html-export-to-html --kill
	mv $@ public/

%.pdf: %.tex
	latexmk -pdf $<
	mv $@ public/

tarball:
	git archive HEAD --format tgz --prefix automation-`git rev-parse HEAD`/ -o automation-`git rev-parse --short HEAD`.tgz

clean:
	rm -f $(addprefix sops., ${GENERATED})

distclean:
	rm -f $(addprefix sops., ${ALL_GENERATED})



